@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>Datos personales</h1>
            <table class="table">
            <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Apellido Paterno</th>
            <th>Apellido Materno</th>
            <th>Fecha Nacimiento</th>
            </tr>
            @foreach($datospersonales as $datosper)
            <tr>
            <td>{{$datosper->id}}</td>
            <td>{{$datosper->nombre}}</td>
            <td>{{$datosper->apellidopaterno}}</td>
            <td>{{$datosper->apellidomaterno}}</td>
            <td>{{$datosper->fechadenacimiento}}</td>
            
            </tr>

            @endforeach
            

            </table>
        </div>
    </div>
            
</div>
@endsection
