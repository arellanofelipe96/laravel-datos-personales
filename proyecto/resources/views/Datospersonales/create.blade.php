@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>Create</h1>
            <form action="{{route('datospersonales.store')}}" method="post">
        @csrf
        <div class="container">
            <div class="form-group">
                <input type="text" class="form-control" name="nombre" placeholder="Nombre">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="apellidopaterno" placeholder="Apellido Paterno">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="apellidomaterno" placeholder="Apellido Materno">
            </div>
            <div class="form-group">
                <input type="date" class="form-control" name="fechadenacimiento" placeholder="Fecha Nacimiento">
            </div>
            <button type="submit" class="btn btn-primary">Guardar</button>
        </div>

    </form>
@endsection