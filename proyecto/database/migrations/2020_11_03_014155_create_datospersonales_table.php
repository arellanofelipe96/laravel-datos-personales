<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatospersonalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datospersonales', function (Blueprint $table) {
            $table->id();
            $table->string('nombre',100);
            $table->string('apellidopaterno',100);
            $table->string('apellidomaterno',100);
            $table->date('fechadenacimiento');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datospersonales');
    }
}
